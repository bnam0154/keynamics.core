# Boldizsar Norbert Benjamin
# Cross Validation Module
# --------------------------------
from sklearn.model_selection import cross_val_score
import pickle
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression

import numpy as np
import SQLite3module as sqlite

# --------------------------------
def crossValScore(dataset, labelset, classifier_obj, cv, scoring):

    # classifier = pickle.loads(classifier_obj)
    classifier = classifier_obj
    score = cross_val_score(classifier, dataset, labelset, cv=cv, scoring=scoring)

    return score

# --------------------------------
def main():
    print("[Cross Validation Module]")
    # get all data from database users
    all_traning_data = []
    all_training_labels = []
    count = 0
    for userData in sqlite.selectAllUsersKeystroke(r'keynamicsSQL3.db'):
        count += 1
        # shapenr = sqlite.selectOneNrDataPerUser(r'keynamicsSQL3.db', shapenr)
        if len(all_traning_data) == 0:
            all_traning_data = np.frombuffer(userData[0]).reshape(-1, 51)
        else:
            all_traning_data = np.append(all_traning_data, np.frombuffer(userData[0]).reshape(-1, 51), 0)

    # a tanulasi cimkek lekerese
    for userData in sqlite.selectAllUserId(r'keynamicsSQL3.db'):
        print(userData[0])
        count = [x for x in sqlite.selectOneNrDataPerUser(r'keynamicsSQL3.db', userData[0])][0][0]
        all_training_labels.extend(np.repeat(userData[0], count))

    all_training_labels = np.array(all_training_labels)

    norlamized_training_data = all_traning_data
    norlamized_training_data = norlamized_training_data / np.linalg.norm(norlamized_training_data, axis=-1)[:, np.newaxis]

    print(len(all_traning_data))
    print(all_traning_data.shape)
    print(all_training_labels.shape)
    print(all_training_labels)

    score = crossValScore(all_traning_data, all_training_labels, GaussianNB(), 3, 'accuracy')
    print("GaussianNB() = ",score)

    score = crossValScore(norlamized_training_data, all_training_labels, MLPClassifier(max_iter=1000), 3, 'accuracy')
    print("MLPClassifier(max_iter=500) = ",score)

    score = crossValScore(all_traning_data, all_training_labels, LinearSVC(), 3, 'accuracy')
    print("LinearSVC() = ",score)

    score = crossValScore(norlamized_training_data, all_training_labels, LogisticRegression(), 3, 'accuracy')
    print("LogisticRegression() = ",score)
    

if __name__ == '__main__':
    main()

# --------------------------------