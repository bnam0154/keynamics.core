# Boldizsar Norbert Benjamin
# --------------------------
# read in data for words

# --------------------------
'''
Beolvassa az adatokat
Visszaterit harom listat:
    - > adatok a felhasznalorol
    - > billentyuzettavolsagokat
    - > billentyuzet letartasi idoket
'''
def readInData(filePath):
    with open(filePath, "r") as fr:
        data = []
        details = fr.readline()
        data = fr.readline().split("],[")
        data[0] = data[0][2:]
        data[len(data)-1] = data[len(data)-1][:-3]
        data = [[float(y) for y in x.split(",")] for x in data]

        keydowupdata = fr.readline().split("]],[[")
        
        keydowupdata[0] = keydowupdata[0][3:]
        keydowupdata[len(keydowupdata)-1] = keydowupdata[len(keydowupdata)-1][:-3]
        keys = []
        for i in range(len(keydowupdata)):
            key=[]
            keydowupdata[i] = (keydowupdata[i]).split('],[')
            for j in range(18):
                key.append(float(keydowupdata[i][j].split(",")[1]))
            keys.append(key)

        keydowupdata = keys
        
    return details, data, keydowupdata
# --------------------------
def parseData(rawData):

    details, data, keydowupdata = rawData.split('\\n')
    data = data.split("],[")
    data[0] = data[0][2:]
    data[len(data)-1] = data[len(data)-1][:-3]
    data = [[float(y) for y in x.split(",")] for x in data]

    keydowupdata = keydowupdata[0:len(keydowupdata)-1]
    keydowupdata = keydowupdata.split("]],[[")
        
    keydowupdata[0] = keydowupdata[0][3:]
    keydowupdata[len(keydowupdata)-1] = keydowupdata[len(keydowupdata)-1][:-3]
    keys = []
    for i in range(len(keydowupdata)):
        key=[]
        keydowupdata[i] = (keydowupdata[i]).split('],[')
        for j in range(18):
            key.append(float(keydowupdata[i][j].split(",")[1]))
        keys.append(key)

    keydowupdata = keys
        
    return details, data, keydowupdata

# --------------------------
def parseLoginData(rawData):

    details, data, keydowupdata = rawData.split('\\n')

    return data, keydowupdata
# --------------------------
def formatData(details, keydelta, keydowupdata):
    '''
    Beolvasott adatokat atalakitja a gepi tanulo rendszernek megfelelo formatumba
    '''
    result = []

    N = int(str(str(details.split("\"numberofcoll\"")[1]).split(",")[0]).split(":")[1])
    for i in range(N):
        data = []
        data.extend(keydelta[i])
        data.extend(keydowupdata[i])
        mean = getExpectedvalues(N, keydowupdata[i])
        data.extend(mean)
        data.extend(getStandardDeviations(N, keydowupdata[i], mean))
        result.append(data)
        
    return result

# --------------------------
def formatLoginData(details, keydelta, keydowupdata):
    '''
    Beolvasott adatokat atalakitja a gepi tanulo rendszernek megfelelo formatumba
    '''
    result = []
    N = 1
    data = []
    data.extend(keydelta[0])
    data.extend(keydowupdata[0])
    mean = getExpectedvalues(N, keydowupdata[0])
    data.extend(mean)
    data.extend(getStandardDeviations(N, keydowupdata[0], mean))
    result.append(data)
        
    return result

# --------------------------
def getExpectedvalues(N, keystroke):

    # <i> <n> <d> <u> <l> <a> <space> <p>
    X = [] 
    X.append((keystroke[0] + keystroke[N])/2)
    X.append((keystroke[1] + keystroke[N-1])/2)
    X.append((keystroke[2] + keystroke[N-2])/2)
    X.append((keystroke[3] + keystroke[N-3])/2)
    X.append((keystroke[4] + keystroke[N-4])/2)
    X.append((keystroke[5] + keystroke[7] + keystroke[11])/2)
    X.append((keystroke[6] + keystroke[9] + keystroke[12])/2)
    X.append((keystroke[8] + keystroke[10])/2)
    
    return X
# --------------------------
def getStandardDeviations(N, keystroke, mean):

    # <i> <n> <d> <u> <l> <a> <space> <p>
    D = []
    for i in range(len(mean)):
        D.append( (keystroke[i]-mean[i]) ** 2 )

    return D
# --------------------------
def main():
   pass

if __name__ == '__main__':
    main()
# --------------------------