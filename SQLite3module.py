import sqlite3
from sqlite3 import Error
import os.path
import csv
# ---------------------------

# ---------------------------
def create_new_database(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()


# ---------------------------

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


# ---------------------------

def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)

# ---------------------------
def create_user(conn, user):
    """
    Create a new project into the projects table
    :param conn:
    :param project:
    :return: project id
    """
    sql = ''' INSERT INTO users(name,keystroke_data, totalnrdata)
              VALUES(?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, user)
    conn.commit()
    cur.close()
    return cur.lastrowid

# ---------------------------

def create_classifier(conn, cl):

    sql = ''' INSERT OR REPLACE INTO trained_data(classifier_name,classifier_obj)
              VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, cl)
    return cur

# ---------------------------
def create_login(conn, login):

    sql = ''' INSERT INTO 
                    nrlogins(userid, totallogins, correctlogins, incorrectlogins)
                    VALUES(?,?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, login)
    conn.commit()
    cur.close()
    return cur.lastrowid

# ---------------------------
def create_untrained_data(conn, data):

    sql = ''' INSERT INTO 
                    untrained_data(keystroke_label, keystroke_data)
                    VALUES(?,?) '''
    cur = conn.cursor()
    cur.execute(sql, data)
    return cur

# ---------------------------

def createNewDatabase(dbname):
    # create database
    create_new_database(dbname)

    sql_create_users_table = """ CREATE TABLE IF NOT EXISTS users (
                                        id integer PRIMARY KEY,
                                        name text NOT NULL,
                                        keystroke_data text, 
                                        totalnrdata integer
                                    ); """


    sql_create_nrlogins_table = """ CREATE TABLE IF NOT EXISTS nrlogins (
                                        id integer PRIMARY KEY,
                                        userid integer NOT NULL,
                                        totallogins integer,
                                        correctlogins integer,
                                        incorrectlogins integer
                                    ); """


    sql_create_trained_data_table = """ CREATE TABLE IF NOT EXISTS trained_data (
                                        id integer PRIMARY KEY,
                                        classifier_name text,
                                        classifier_obj BLOB
                                    ); """

    sql_create_unique_index_trained_data_table = """ CREATE UNIQUE INDEX idx_classifier_name
                                                     ON trained_data (classifier_name);

                                                 """

    sql_create_untrained_data_table = """ CREATE TABLE IF NOT EXISTS untrained_data (
                                        id integer PRIMARY KEY,
                                        keystroke_label integer, 
                                        keystroke_data text
                                    ); """

    # create a database connection
    conn = create_connection(dbname)

    # create table
    if conn is not None:
        # create "users" table
        create_table(conn, sql_create_users_table)
        create_table(conn, sql_create_nrlogins_table)
        create_table(conn, sql_create_trained_data_table)
        create_table(conn, sql_create_unique_index_trained_data_table)
        create_table(conn, sql_create_untrained_data_table)

    else:
        print("[Error: cannot create the database connection]")

# ---------------------------
def updateLoginData(dbname,count, userId):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        # insert syntax for sql
        cur = conn.cursor()
        update = cur.execute("""UPDATE users SET totalnrdata = totalnrdata + ?
                                                  WHERE id = ?
                                                  """, (count,userId,))
        conn.commit()
        cur.close()

# ---------------------------
def insertLoginData(dbname, userid, totallogins, correctlogins, incorrectlogins):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        # insert syntax for sql
        user = create_login(conn, (userid,totallogins,correctlogins,incorrectlogins, ))

# ---------------------------
def insertUserIntoDatabase(dbname, name, keystroke_data, nrdata):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        # insert syntax for sql
        user = create_user(conn, (name,keystroke_data,nrdata, ))

    return user

# ---------------------------
def updateUserKeystroke(dbname, data, userid):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("""UPDATE users SET keystroke_data = ?
                                                  WHERE id = ?
                                                  """, (data,userid,)).fetchall()
        conn.commit()
        cur.close()
        
    return select
    
# ---------------------------
def selectAllLoginData(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT * FROM nrlogins").fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def selectAllUsers(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT * FROM users").fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def selectAllUsersKeystroke(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT keystroke_data FROM users").fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def selectAllUserId(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT id FROM users").fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def selectAllUserNames(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT name FROM users").fetchall()
        conn.commit()
        cur.close()

    return select

# ---------------------------
def countAllUsers(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        count = cur.execute("SELECT COUNT(id) FROM users").fetchall()
        conn.commit()
        cur.close()

    return count

# ---------------------------
def selectUserNameByID(dbname, id):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT name FROM users WHERE id = {id}".format(id=id)).fetchall()
        conn.commit()
        cur.close()
        
    return select


# ---------------------------
def selectAllNrOfDataPerUser(dbname):

     # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT totalnrdata FROM users").fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def selectOneNrDataPerUser(dbname, userid):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT totalnrdata FROM users WHERE id=?", (userid,)).fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def selectOneUser(dbname, userid):
    
    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT keystroke_data FROM users WHERE id=?", (userid,)).fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def saveDBToCSV(dbname, table):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT * FROM {table}".format(table=table)).fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def loadDBFromCSV(dbname, table):

    # create a database connection
    conn = create_connection(dbname)

    # load csv data
    with open('{table}.csv'.format(table=table), 'r') as fr:
        data = csv.DictReader(fr)
        todb = [(i['id'], i['name'], i['keystroke_data']) for i in data]

    # insert into database table
    with conn:
        cur = conn.cursor()
        cur.execute("INSERT INTO {table} (id, name, keystroke_data) VALUES (?,?,?);".format(table=table), todb)
        cur.close()

# ---------------------------
def deleteAllUsers(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("DELETE FROM users")
        conn.commit()
        cur.close()

# ---------------------------
def dropUsersTable(database):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("DROP TABLE users")
        conn.commit()
        cur.close()
    
# ---------------------------
def initiateDatabaseIfNeeded(dbname):

    print('[Checking if db exists]')
    if os.path.exists('./{dbname}'.format(dbname=dbname)):
        print('[Database exists: ok]')
    else:    
        print('[Database does not exist, creating db]')
        createNewDatabase(dbname)

# ---------------------------
def updateNrLogins(dbname, isCorrect, userId):

     # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        if isCorrect:
            select = cur.execute("""UPDATE nrlogins SET totallogins = totallogins + 1,
                                                  correctlogins = correctlogins + 1
                                                  WHERE userid = ?
                                                  """, (userId,))
        else:
            select = cur.execute("""UPDATE nrlogins SET totallogins = totallogins + 1,
                                                  incorrectlogins = incorrectlogins + 1
                                                  WHERE userid = ?
                                                  """, (userId,))
        conn.commit()
        cur.close()


# ---------------------------
def insertIntoTrainedData(dbname, name, obj):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        # insert syntax for sql
        cur = create_classifier(conn, (name,obj, ))
        conn.commit()

# ---------------------------
def getTrainedData(dbname, name):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        obj = cur.execute("SELECT classifier_obj FROM trained_data WHERE classifier_name = ?", (name,)).fetchall()
        conn.commit()
        cur.close()

    return obj

# ---------------------------
def insertUntrainedData(dbname, keystroke_data, label):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        # insert syntax for sql
        cur = create_untrained_data(conn, (label, keystroke_data, ))
        conn.commit()
        cur.close()

# ---------------------------
def deleteAllUntrainedData(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        delete = cur.execute("DELETE FROM untrained_data").fetchall()
        conn.commit()
        cur.close()
        
# ---------------------------
def selectAllUntrainedData(dbname):

    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        select = cur.execute("SELECT * FROM untrained_data").fetchall()
        conn.commit()
        cur.close()
        
    return select

# ---------------------------
def countAllUntrainedData(dbname):

    
    # create a database connection
    conn = create_connection(dbname)

    with conn:
        cur = conn.cursor()
        count = cur.execute("SELECT COUNT(*) FROM untrained_data").fetchall()
        conn.commit()
        cur.close()
        
    return count

# ---------------------------

def main():
    # database = r"pythonsqlite.db"
    # users = selectAllUsers(database)
    pass
# ---------------------------

if __name__ == '__main__':
    main()
    
# ---------------------------