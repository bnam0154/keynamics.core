# Boldizsar Norbert Benjamin
# main python file for keynamicsServer

# ---------------------------------------------------------

import os
from flask import Flask
from flask import request, make_response, jsonify, Response
import numpy as np
keynamicsServer = Flask(__name__)

import dataModule as dm
import SQLite3module as sqlite
import classifications as cl

# ---------------------------------------------------------
@keynamicsServer.route('/resetdatabase', methods=['GET'])
def resetdatabase():

    # reset database
    sqlite.deleteAllUsers(r'keynamicsSQL3.db')

    response = {
        'message': 'Reseted database'
    }
    return make_response(jsonify(response,200))

# ---------------------------------------------------------
@keynamicsServer.route('/savedbtocsv', methods=['GET'])
def savedbtocsv():

    # TO DO: kijavitani: bites like obj not int required

    # number of data collected
    allNumberOfData = []
    for nr in sqlite.selectAllNrOfDataPerUser(r'keynamicsSQL3.db'):
        allNumberOfData.append(nr[0])

    allData = []
    i = 0
    for data in sqlite.saveDBToCSV(r'keynamicsSQL3.db', 'users'):
        allData.append([ data[0], str(data[1]), str(data[2]), data[3]])
        i += 1
    
    response = {
        'message': 'db selected',
        'alldata': allData
    }
    return make_response(jsonify(response,200))

# ---------------------------------------------------------
'''
getinfodb()
--------------
- > Description:Send information about the currenct database
'''
@keynamicsServer.route('/getinfodb', methods=['GET'])
def getinfodb():

    dbname = r'keynamicsSQL3.db'

    # Nr of users registered
    totalNrOfUsers = sqlite.countAllUsers(dbname)[0][0]
    print(totalNrOfUsers)

    # list of registered use id
    allUserId = []
    for userName in sqlite.selectAllUserId(dbname):
        allUserId.append(userName[0])

    # list of registered user names
    allUserNames = []
    for userName in sqlite.selectAllUserNames(dbname):
        allUserNames.append(userName[0])

    # number of data collected
    allNumberOfData = []
    for nr in sqlite.selectAllNrOfDataPerUser(dbname):
        allNumberOfData.append(nr[0])

    # login informations
    loginInformation = []
    for login in sqlite.selectAllLoginData(dbname):
        loginInformation.append([login[1], login[2], login[3], login[4]])

    # untrained data number
    for count in sqlite.countAllUntrainedData(r'keynamicsSQL3.db'):
        nrAllUntrainedData = count


    response = {
        'message': 'Information of current db',
        'totalNrOfUsers': totalNrOfUsers,
        'allUserId': allUserId,
        'allUserNames': allUserNames,
        'allNumberOfData': allNumberOfData,
        'loginInformation': loginInformation,
        'nrAllUntrainedData': nrAllUntrainedData
    }

    return make_response(jsonify(response,200))
    
# ---------------------------------------------------------
def trainData(dbname):

    # get all data from database users
    all_traning_data = []
    all_training_labels = []
    count = 0
    for userData in sqlite.selectAllUsersKeystroke(r'keynamicsSQL3.db'):
        count += 1
        if len(all_traning_data) == 0:
            all_traning_data = np.frombuffer(userData[0]).reshape(-1, 51)
        else:
            all_traning_data = np.append(all_traning_data, np.frombuffer(userData[0]).reshape(-1, 51), 0)

    # a tanulasi cimkek lekerese
    for userData in sqlite.selectAllUserId(r'keynamicsSQL3.db'):
        count = sqlite.selectOneNrDataPerUser(r'keynamicsSQL3.db', userData[0])
        all_training_labels.extend(np.repeat(userData[0], count[0]))

    all_training_labels = np.array(all_training_labels)

    # train data and save parameters to db
    classifier_obj = cl.train_gaussianNaiveBayes(all_traning_data, all_training_labels)
    sqlite.insertIntoTrainedData(r'keynamicsSQL3.db', "gaussianNaiveBayes", classifier_obj)
    

# ---------------------------------------------------------
'''
dataFromClient()
--------------
 - > Description: Receives HTTP POST from client, serving
     the keystroke information of the given user.
     Saves the data into database.
 - > Parameters: None
 - > ReturnVale: 200 if data has been received
'''
@keynamicsServer.route('/keystrokesdata', methods=['POST'])
def dataFromClient():
    print("[Data received from client]")
    # print(request.data)
    details, keydelta, keydowupdata = dm.parseData(str(request.data))
    
    # adatok standard alakba hozasa
    registrationUserData = dm.formatData(details, keydelta, keydowupdata)
    # normalizalasa [0,1] intervallumra
    # registrationUserData = registrationUserData / np.linalg.norm(registrationUserData, axis=-1)[:, np.newaxis]
    registrationUserData = np.array(registrationUserData)
    # adatok elmentese az adatbazisba mint uj felhasznalo
    user_name = str(details.split(',')[0]).split(':')[1].strip('\"')
    
    registrationUserData = registrationUserData.tostring()

    userid = sqlite.insertUserIntoDatabase(r'keynamicsSQL3.db', user_name, registrationUserData, 10)
    sqlite.insertLoginData(r'keynamicsSQL3.db', userid, 0, 0, 0)

    # train extended data
    trainData(r'keynamicsSQL3.db')


    return make_response(jsonify({'message':'Success'},200))


# ---------------------------------------------------------
@keynamicsServer.route('/login', methods=['POST'])
def login():
    print("[Login data received from client]")
    details, keydelta, keydowupdata = dm.parseData(str(request.data))
    # adatok standard alakba hozasa
    loginUserData = dm.formatLoginData(details, keydelta, keydowupdata)

    # normalizalasa [0,1] intervallumra
    # loginUserData = loginUserData / np.linalg.norm(loginUserData, axis=-1)[:, np.newaxis]
    loginUserData = np.array(loginUserData)
    print(len(loginUserData))
    loginUserData = loginUserData.reshape(1,-1)

    # get parameters for classifier
    classifier_obj = sqlite.getTrainedData(r'keynamicsSQL3.db', 'gaussianNaiveBayes')
    # run test
    test_labels, score, log_prob, prob = cl.test_gaussianNaiveBayes([x for x in classifier_obj][0][0], loginUserData)

    for username in sqlite.selectUserNameByID(r'keynamicsSQL3.db', test_labels[0]):
        print(username)
        result = str(username).split('\'')[1]  

    # result = str(test_labels) 
    return make_response(jsonify({'message':'Success'},{'predicted_id':str(test_labels[0])},{'predicted_user': result}, {'score': str(score)}, {'log_prob': str(log_prob)}, {'prob': str(prob)} ,200))
# ---------------------------------------------------------
@keynamicsServer.route('/loginfeedback', methods=['POST'])
def loginfeedback():

    requestdata = [int(str(str(request.data).split(',\"')[0].strip('b\'['))), int(str(str(request.data).split(',\"')[1].strip('\"]\\\'')))]

    sqlite.updateNrLogins(r'keynamicsSQL3.db', requestdata[0], int(requestdata[1]))
    
    return make_response(jsonify({'message':'Success'},200))

# ---------------------------------------------------------
@keynamicsServer.route('/addtountraineddata', methods=['POST'])
def addtountraineddata():
    print("[Keystroke added to untrained data]")

    data = '\\n' + str(request.data).split('|')[0].strip('b\'')
    
    details, keydelta, keydowupdata = dm.parseData(data)
    loginUserData = dm.formatLoginData(details, keydelta, keydowupdata)
    loginUserData = np.array(loginUserData)
    loginUserData = loginUserData.reshape(1,-1)

    label = int(str(str(request.data).split('|')[1].strip('\'')).strip('\"'))

    # add data to untrained_data list
    sqlite.insertUntrainedData(r'keynamicsSQL3.db', loginUserData.tostring(), label)

    return make_response(jsonify({'message':'Success'},200))
# ---------------------------------------------------------
@keynamicsServer.route('/trainuntraineddata', methods=['GET'])
def trainuntraineddata():
    print("[Training untrained data]")

    # get all data from database users
    all_traning_data = []
    all_training_labels = []
    count = 0
    for userData in sqlite.selectAllUsersKeystroke(r'keynamicsSQL3.db'):
        count += 1
        if len(all_traning_data) == 0:
            all_traning_data = np.frombuffer(userData[0]).reshape(-1, 51)
        else:
            all_traning_data = np.append(all_traning_data, np.frombuffer(userData[0]).reshape(-1, 51), 0)

    # a tanulasi cimkek lekerese
    for userData in sqlite.selectAllUserId(r'keynamicsSQL3.db'):
        count = sqlite.selectOneNrDataPerUser(r'keynamicsSQL3.db', userData[0])
        all_training_labels.extend(np.repeat(userData[0], count[0]))

    for data in sqlite.selectAllUntrainedData(r'keynamicsSQL3.db'):
        all_traning_data = np.append(all_traning_data, np.frombuffer(data[2]).reshape(-1, 51), 0)
        all_training_labels.extend(np.repeat(data[1], 1))
        sqlite.updateLoginData(r'keynamicsSQL3.db', 1, data[1])
        currentdata = np.frombuffer([x for x in sqlite.selectOneUser(r'keynamicsSQL3.db', data[1])][0][0]).reshape(-1, 51)
        newdata = np.append(currentdata, np.frombuffer(data[2]).reshape(-1, 51), 0)
        sqlite.updateUserKeystroke(r'keynamicsSQL3.db', newdata.tostring(), data[1])

    all_training_labels = np.array(all_training_labels)

    # train data and save parameters to db
    classifier_obj = cl.train_gaussianNaiveBayes(all_traning_data, all_training_labels)
    sqlite.insertIntoTrainedData(r'keynamicsSQL3.db', "gaussianNaiveBayes", classifier_obj)

    # clear untraineddata table
    sqlite.deleteAllUntrainedData(r'keynamicsSQL3.db')

    return make_response(jsonify({'message':'Success'},200))

# ---------------------------------------------------------
'''
main()
--------------
 - > Description: Main function of server.
 - > Parameters: None
 - > ReturnVale: None
'''
def main():

    # letrehozzuk az adatbazist a megfelelo tablakkal
    sqlite.initiateDatabaseIfNeeded(r'./keynamicsSQL3.db')


    port = int(os.environ.get("PORT", 5000))
    keynamicsServer.run(host='0.0.0.0', port=port)

if __name__ == '__main__':
    main()
# ------------------------------