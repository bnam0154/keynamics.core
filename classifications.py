# Boldizsar Norbert Benjamin
# --------------------------
import dataModule as dm
from sklearn.svm import OneClassSVM
from sklearn.naive_bayes import GaussianNB
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LinearRegression
import numpy as np
import pickle
# from learningdata import *

# --------------------------
def test_gaussianNaiveBayes(classifier_obj, test_data):

    gnb = pickle.loads(classifier_obj)

    predicted_label = gnb.predict(test_data)
    score = gnb.score(test_data, predicted_label)
    log_prob = gnb.predict_log_proba(test_data)
    prob = gnb.predict_proba(test_data)

    return predicted_label, score, log_prob, prob

# --------------------------
def train_gaussianNaiveBayes(train_data, train_labels):

    gnb = GaussianNB()
    gnb = gnb.fit(train_data, train_labels)
    classifier_obj = pickle.dumps(gnb)

    return classifier_obj

# --------------------------
def decisionTreeClassifier(train_data, train_labels, test_data):

    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(train_data, train_labels)
    predicted_label = clf.predict(test_data)
    prob = clf.predict_proba(test_data)

    return predicted_label, None, None, prob

# --------------------------
def kNeighborsClassifier(train_data, train_labels, test_data):

    neigh = KNeighborsClassifier()
    neigh = neigh.fit(train_data, train_labels)
    predicted_label = neigh.predict(test_data)
    score = neigh.score(test_data, predicted_label)
    prob = neigh.predict_proba(test_data)
    
    return predicted_label, score, None, prob

# --------------------------
def discriminantAnalysis(train_data, train_labels, test_data):

    dis = LinearDiscriminantAnalysis()
    dis = dis.fit(train_data, train_labels)
    predicted_label = dis.predict(test_data)
    score = dis.score(test_data, predicted_label)
    # log_prob = dis.predict_log_proba(test_data)
    prob = dis.predict_proba(test_data)

    return predicted_label, score, None, prob    

# --------------------------
def multiLayerPeceptron(train_data, train_labels, test_data):

    mlp = MLPClassifier(max_iter=500)
    mlp = mlp.fit(train_data, train_labels)
    predicted_label = mlp.predict(test_data)
    score = mlp.score(test_data, predicted_label)
    log_prob = mlp.predict_log_proba(test_data)
    prob = mlp.predict_proba(test_data)

    return predicted_label, score, log_prob, prob 

# --------------------------
def linearRegression():

    return predicted_label, score, None, None

# --------------------------
def logisticRegression():

    pass

# --------------------------
def linearSVC():

    pass

# --------------------------
def main():
    pass
    
if __name__ == '__main__':
    main()
# --------------------------

